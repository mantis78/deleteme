package main

import (
	"github.com/fsnotify/fsnotify"
	"log"
	"os"
	"strings"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: deleteme <dir>")
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				log.Println("event:", event)
				if event.Op&fsnotify.Chmod == fsnotify.Chmod || event.Op&fsnotify.Write == fsnotify.Write {
					log.Println("modified file:", event.Name)
					if strings.HasSuffix(event.Name, ".delete") {
						var fileToDelete = strings.ReplaceAll(event.Name, ".delete", "")
						log.Printf("Detected file that needs deleting: %s\n", fileToDelete)
						err = os.Remove(event.Name)
						if err != nil {
							log.Fatal(err)
						}
						err = os.Remove(fileToDelete)
						if err != nil {
							log.Fatal(err)
						}
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	err = watcher.Add(os.Args[1])
	log.Printf("Watching %s", os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	<-done
}
